package com.atfortecdynamics.studentreg.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.atfortecdynamics.studentreg.R;

/**
 * Created by folio on 10/23/2018.
 */

public class ListViewAdapterWithArrays extends BaseAdapter {

    String[] name;
    String[] age;
    String[] gender;
    String[] course;

    Context context;
    LayoutInflater inflater;

    /*
    adding a constructor will help us get the data we need

    * */

    public ListViewAdapterWithArrays(Context context,String[] name,
                                     String[] age, String[] gender, String[] course) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.course = course;
        this.context = context;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        // create our view from the inflater inorder to give it the data required
        view = inflater.inflate(R.layout.custom_row,null);

        // creating objects of the widgets found in the view i.e th textviews
        TextView tx_name = view.findViewById(R.id.txt_name);
        TextView tx_age = view.findViewById(R.id.txt_age);
        TextView tx_gender = view.findViewById(R.id.txt_gender);
        TextView tx_course = view.findViewById(R.id.txt_course);

        //parsing data to our textvew by giving the view a respective index to the array
        tx_age.setText(age[i]);
        tx_course.setText(course[i]);
        tx_gender.setText(gender[i]);
        tx_name.setText(name[i]);

        return view;
    }
}
