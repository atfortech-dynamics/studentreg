package com.atfortecdynamics.studentreg.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.atfortecdynamics.studentreg.DO.Student;
import com.atfortecdynamics.studentreg.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by folio on 10/23/2018.
 */

public class ListViewAdapterWithHashMaps extends BaseAdapter{
    Context context;
    HashMap<Long,Student> data;
    LayoutInflater inflater;

    public ListViewAdapterWithHashMaps(Context context, HashMap<Long,Student> data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        // get the count of the items residing inside th list
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // create our view from the inflater inorder to give it the data required
        view = inflater.inflate(R.layout.custom_row,null);

        // creating objects of the widgets found in the view i.e th textviews
        TextView tx_name = view.findViewById(R.id.txt_name);
        TextView tx_age = view.findViewById(R.id.txt_age);
        TextView tx_gender = view.findViewById(R.id.txt_gender);
        TextView tx_course = view.findViewById(R.id.txt_course);

        tx_name.setText(data.get(i).getName());
        tx_age.setText(String.valueOf(data.get(i).getAge()));
        tx_gender.setText(data.get(i).getGender());
        tx_course.setText(data.get(i).getCourse());
        return view;
    }
}
