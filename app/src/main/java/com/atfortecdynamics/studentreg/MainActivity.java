package com.atfortecdynamics.studentreg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.atfortecdynamics.studentreg.network.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String strName,strAge,strCourse,strGender;
    EditText edName,edAge,edCourse,edGender;
    Button btn_save,btn_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edName = findViewById(R.id.student_name);
        edAge = findViewById(R.id.student_age);
        edCourse = findViewById(R.id.student_course);
        edGender = findViewById(R.id.student_gender);
        btn_save = findViewById(R.id.btn_save);
        btn_view = findViewById(R.id.btn_view_students);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strAge = edAge.getText().toString();
                strCourse = edCourse.getText().toString();
                strGender = edCourse.getText().toString();
                strName = edName.getText().toString();

                if(!strName.isEmpty() && !strGender.isEmpty() && !strCourse.isEmpty() && !strAge.isEmpty()){
                    // data exists in the edit texts
                    new saveStudentData().execute();
                }else{
                    Toast.makeText(getApplicationContext(),"insert all information",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,ViewStudents.class));

            }
        });

    }

    public class saveStudentData extends AsyncTask<String,String,String>{

        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        int status;
        String message;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("uploading data. please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {

            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("name",strName));
            params.add(new BasicNameValuePair("age",strAge));
            params.add(new BasicNameValuePair("course",strCourse));
            params.add(new BasicNameValuePair("gender",strGender));

            JSONObject jsonObject = jsonParser.makeHttpRequest(config.hostname+config.student_registration,params);
            try{
                status = jsonObject.getInt("status");
                message = jsonObject.getString("message");

            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String s){
            super.onPostExecute(s);
            dialog.dismiss();
            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
        }


    }
}
