package com.atfortecdynamics.studentreg;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.atfortecdynamics.studentreg.Adapters.ListViewAdapterWithArrays;
import com.atfortecdynamics.studentreg.Adapters.ListViewAdapterWithHashMaps;
import com.atfortecdynamics.studentreg.Adapters.ListViewAdapterWithList;
import com.atfortecdynamics.studentreg.DO.Student;
import com.atfortecdynamics.studentreg.network.JSONParser;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by folio on 10/24/2018.
 */

public class ViewStudents extends AppCompatActivity {

    ListView listView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fetch_students);

        listView=findViewById(R.id.list_of_students);

        new loadStudents().execute();
    }

    public class loadStudents extends AsyncTask<String,String,String>{

        int status;
        String message;
        JSONArray data;
        ProgressDialog dialog = new ProgressDialog(ViewStudents.this);
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("loading student data. please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            // call our network controller class
            JSONParser jsonParser = new JSONParser();
            // we make a request to the server for a json object response
            JSONObject jsonObject = jsonParser.makeHttpRequest(config.hostname+config.fetch_students);

            try {
                status = jsonObject.getInt("status");
                message = jsonObject.getString("message");

                /* if the status is a positive response i.e fetched data we can proceed
                to decode the json array data else we will avoid doing so inorder to avoid our code crushing
                 */
                if(status ==1){
                    data = jsonObject.getJSONArray("data");

                    /*we proceed by calling our decoder class at this point because we have data
                        of type jsonArray
                     */
                    new ArrayDecoder(data).loadArrays();
                    new ArrayDecoder(data).loadList();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s){
            super.onPostExecute(s);

            /* you need to check for your status response in order
                to avoid adding null objects to your adapter
             */
            if(status==1){
                ListViewAdapterWithArrays arrayAdapter = new ListViewAdapterWithArrays(ViewStudents.this,
                        config.name,config.age,config.gender,config.course);

                ListViewAdapterWithList listAdapter = new ListViewAdapterWithList(ViewStudents.this,
                        config.studentList);

                ListViewAdapterWithHashMaps hashAdapter = new ListViewAdapterWithHashMaps(ViewStudents.this,
                        config.studentHashMap);

                listView.setAdapter(listAdapter);
            }

            dialog.dismiss();
            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
        }
    }

    public class ArrayDecoder{
        JSONArray jsonArray;
        public ArrayDecoder(JSONArray jsonArray) {
            this.jsonArray=jsonArray;
        }

        /* this class will be responsible for adding data to our arrays found in config class
            i.e String[] name,age,gender,course
         */
        public void loadArrays() throws JSONException {
                /* when adding to an array , you are supposed to give the lenght of the array
                    i.e String[] array = new String[length] -> because it is defined
                 */

                config.age = new String[jsonArray.length()];
                config.name = new String[jsonArray.length()];
                config.gender = new String[jsonArray.length()];
                config.course = new String[jsonArray.length()];

                /* create a loop that will enable us add data to our arrays from the json array given
                 */
                for(int i=0;i<jsonArray.length();i++){
                    // create an object from the json objects found in the json array
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    /* reading the json object and populating the data to our respective arrays
                        for you to read the json object, you are supposed to give the name of the
                        data/value you want to retrieve
                        e.g if our json object object is {"name":"roy"}
                        1. you have to specify the data type you are getting from the json object
                            by using jsonObject.get{DataType:Int,String,Boolean};

                            using jsonObject.get() only would return a object  which you will have to cast later

                     */
                    config.course[i]=jsonObject.getString("course");

                    /* age is of type integer i.e we have to convert it back to a string because
                    our array is of type String
                     */
                    config.age[i]= String.valueOf(jsonObject.getInt("age"));
                    config.name[i]=jsonObject.getString("name");
                    config.gender[i]=jsonObject.getString("gender");


                }
        }

        public void loadList() throws JSONException {
            /*we need to create a new instance of a List<Students>
            everytime the load list is called in order to populate
            it with fresh data
             */
            config.studentList = new ArrayList<>();
            config.studentHashMap = new HashMap<>();

            /* call the loop to populate data to our hashmap and arraylist

             */

            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                /* we need to create a new  object of student that we will add the json object contents
                    to the list and hashmap

                    everytime we read the json object in the loop we need to create a new object of student
                 */
                Student student = new Student();

                int age = jsonObject.getInt("age");
                String course = jsonObject.getString("course");


                student.setAge(age);
                student.setCourse(course);
                student.setGender(jsonObject.getString("gender"));
                student.setName(jsonObject.getString("name"));
                student.setId(jsonObject.getInt("id"));

                // add our student object our list and hashmap

                //list
                config.studentList.add(student);
                //hashmap
                config.studentHashMap.put((long) i,student);
            }

        }
    }
}
