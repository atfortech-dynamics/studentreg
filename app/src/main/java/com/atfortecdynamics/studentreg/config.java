package com.atfortecdynamics.studentreg;

import com.atfortecdynamics.studentreg.DO.Student;

import java.util.HashMap;
import java.util.List;

/**
 * Created by folio on 10/19/2018.
 */

public class config {

    // we store our network file/ urls for our APIs
    public static String hostname = "https://zal.mauzoafrica.com/";
    public static String student_registration = "student-registration.php";
    public static String fetch_students="fetch-students.php";

    //storage for data recieved from our online storage
    public static String[] name;
    public static String[] age;
    public static String[] course;
    public static String[] gender;

    public static List<Student> studentList;
    public static HashMap<Long,Student> studentHashMap;


}
