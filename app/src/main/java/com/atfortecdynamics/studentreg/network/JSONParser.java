package com.atfortecdynamics.studentreg.network;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    public JSONObject makeHttpRequest(String url,@Nullable List<NameValuePair> params) {
         JSONObject jsonObject=new OkJsonRequest().makeRequest(new UrlBuilder().build(url,params));
        return jsonObject;

    }
    public JSONObject makeHttpRequest(String url) {
        JSONObject jsonObject=new OkJsonRequest().makeRequest(url);
        return jsonObject;

    }
}
